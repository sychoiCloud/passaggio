import axios from 'axios'
import router from '@/router'

export default {
    getDataList ({ commit }, tableName) {
        commit('setLoadingStatus', true);
        axios.get(`/api/${ tableName }`)
            .then(response => {
                // console.log(response)
                commit('setDataList', response.data);
                commit('setLoadingStatus', false)
            })
            .catch(error => console.log(error))
    },

    getUserIdList ({ commit }) {
        axios.get(`/api/user/idList`)
            .then(response => {
                commit('setUserIdList', response.data)
            })
            .catch(error => console.log(error))
    },

    getClientNameList ({ commit }) {
        axios.get(`/api/client/nameList`)
            .then(response => {
                commit('setClientNameList', response.data);
                console.log(response.data)
            })
            .catch(error => console.log(error))
    },

    getProductNameList ({ commit }) {
        axios.get(`/api/product/nameList`)
            .then(response => {
                commit('setProductNameList', response.data)
            })
            .catch(error => console.log(error))
    },

    getUserInfo ({ commit }) {
        let userId = localStorage.getItem('user')
        axios.get(`/api/user/${userId}`)
            .then(response => {
                commit('setUserInfo', response.data)
            })
            .catch(error => {
                console.log(error.response.data.message)
            })
    },

    dataListClear ({ commit }, item) {
        commit('setDataList', item)
    },

    pushClickedItem ({ commit }, item) {
        commit('setClickedItem', item)
    },
    pushClickedSecondItem ({ commit }, item) {
        commit('setClickedSecondItem', item)
    },

    pushDefaultItem ({ commit }) {
        commit('initClickedItem')
    },

    pressCreateButton({ commit }, val) {
        commit('setCreateDialog', val)
    },

    pressEditButton({ commit }, val) {
        commit('setEditDialog', val)
    },

    pressShowRequestButton({ commit }, val) {
        commit('setShowRequestDialog', val)
    },

    pressRequestProcessButton({ commit }, val) {
        commit('setRequestProcessDialog', val)
    },

    pressRequestProcessType({ commit }, val) {
        commit('setRequestProcess', val)
    },

    pressUserLicenseHistoryButton({ commit }, val) {
        commit('setUserLicenseHistoryDialog', val)
    },

    pressUserLicenseListButton({ commit }, val) {
        commit('setUserLicenseListDialog', val)
    },

    pressRequestButton({ commit }, val) {
        commit('setRequestDialog', val)
    },

    pressShowLicenseButton({ commit }, val) {
        commit('setShowLicenseDialog', val)
    },

    pressUserPasswordButton({ commit }, val) {
        commit('setUserPasswordDialog', val)
    },

    pressAdminPasswordButton({ commit }, val) {
        commit('setAdminPasswordDialog', val)
    },

    pressResetPasswordButton({ commit }, val) {
        commit('setResetPasswordDialog', val)
    },

    pushCreating({ commit }, val) {
        commit('setIsCreating', val)
    },

    pushEditting({ commit }, val) {
        commit('setIsEditting', val)
    },

    pushRequestProcessing({ commit }, val) {
        commit('setIsRequestProcessing', val)
    },

    pushShowUserLicenseHistory({ commit }, val) {
        commit('setIsShowUserLicenseHistory', val)
    },

    pushShowUserLicenseList({ commit }, val) {
        commit('setIsShowUserLicenseList', val)
    },

    pushDeleteMode({ commit }, val) {
        commit('setDeleteMode', val)
    },

    pushLicenseMode({ commit }, val) {
        commit('setLicenseMode', val)
    },

    pushRequesting({ commit }, val) {
        commit('setIsRequesting', val)
    },

    pushShowLicense({ commit }, val) {
        commit('setIsShowLicense', val)
    },




    openAlertBox ({ commit }, [
        alertType,
        data,
    ]) {
        if ([
            'success',
            'warning',
            'error',
            'info',
        ].includes(alertType)) {
            commit(alertType, data)
        }
        setTimeout(() => {
            commit('closeAlertBox')
        }, 2000)
    },

    // auth
    login ({ commit }, payload) {
        return new Promise((resolve, reject) => {
            commit('auth_request');
            const params = new URLSearchParams();
            params.append('id', payload.id);
            params.append('password', payload.password);
            axios.post(`/token`, params)
                .then(response => {
                    const token = response.data.token;
                    const user = response.data.username;
                    const authorization = response.data.authorization;

                    // storing jwt in localStorage. https cookie is safer place to store
                    localStorage.setItem('token', token);
                    localStorage.setItem('user', user);
                    localStorage.setItem('role', authorization);

                    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                    // mutation to change state properties to the values passed along
                    commit('auth_success', { token, user, authorization });
                    // openAlertBox('Success', 'Login Success !!')
                    location.reload()
                    resolve(response);
                })
                .catch(err => {
                    commit('auth_error');
                    localStorage.removeItem('token');
                    reject(err)
                })
        })
    },

    logout ({ commit }) {
        return new Promise((resolve, reject) => {

            commit('logout')
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            localStorage.removeItem('authorization')
            localStorage.removeItem('adminmode')
            localStorage.removeItem('usermode')

            router.push('/').catch(err => {});
            // location.reload();
            resolve()
        })
    },

    refreshtoken ({ commit }) {
        axios.get('/refresh')
            .then(response => {
                const token = response.data.access_token;
                localStorage.setItem('token', token);
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                commit('auth_success', { token })
            })
            .catch(error => {
                commit('logout');
                localStorage.removeItem('token');
                console.log(error)
            })
    },

    pressSignupButton({ commit }, val) {
        commit('setSignupDialog', val)
    },
}

