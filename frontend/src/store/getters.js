// https://vuex.vuejs.org/en/getters.html

export default {
    authorized: state => !!state.token,
    // authstatus: state => state.authStatus,
    // authrole: state => state.authRole,
    adminmode: state => JSON.parse(state.adminMode),
    usermode: state => JSON.parse(state.userMode)
}
