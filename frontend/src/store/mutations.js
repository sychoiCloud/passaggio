// https://vuex.vuejs.org/en/mutations.html

export default {
    // crud
    setDataList (state, dataList) {
        state.dataList = dataList
    },
    setUserIdList (state, idLIst) {
        state.userIdList = idLIst
    },
    setUserInfo (state, info) {
        info.password = ''
        state.userInfo = info
    },
    setClientNameList (state, nameList) {
        state.clientNameList = nameList
    },
    setProductNameList (state, nameList) {
        state.productNameList = nameList
    },
    setClickedItem (state, item) {
        state.item = item
    },
    setClickedSecondItem (state, item) {
        state.secondItem = item
    },
    initClickedItem (state) {
        state.item = state.defaultItem
    },
    setLoadingStatus (state, val) {
        state.loading = val
    },
    setCreateDialog (state, val) {
        state.createDialog = val
    },
    setEditDialog (state, val) {
        state.editDialog = val
    },
    setDeleteMode (state, val) {
        state.deleteMode = val
    },

    setShowRequestDialog (state, val) {
        state.showRequestDialog = val
    },
    setRequestProcessDialog (state, val) {
        state.requestProcessDialog = val
    },
    setRequestProcess (state, val) {
        state.requestProcess = val
    },
    setUserLicenseHistoryDialog (state, val) {
        state.showUserLicenseHistoryDialog = val
    },
    setUserLicenseListDialog (state, val) {
        state.showUserLicenseListDialog = val
    },
    setShowLicenseDialog (state, val) {
        state.showLicenseDialog = val
    },
    setAdminPasswordDialog (state, val) {
        state.adminPasswordDialog = val
    },
    setUserPasswordDialog (state, val) {
        state.userPasswordDialog = val
    },
    setResetPasswordDialog (state, val) {
        state.resetPasswordDialog = val
    },
    setIsCreating (state, val) {
        state.isCreating = val
    },
    setIsEditting (state, val) {
        state.isEditting = val
    },
    setIsShowRequesting (state, val) {
        state.isShowRequesting = val
    },
    setIsRequestProcessing (state, val) {
        state.isRequestProcessing = val
    },
    setIsShowUserLicenseHistory (state, val) {
        state.isShowUserLicenseHistory = val
    },
    setIsShowUserLicenseList (state, val) {
        state.isShowUserLicenseList = val
    },
    setLicenseMode (state, val) {
        state.licenseMode = val
    },

    setRequestDialog (state, val) {
        state.requestDialog = val
    },
    setIsRequesting (state, val) {
        state.isRequesting = val
    },
    setIsShowLicense (state, val) {
        state.isShowLicense = val
    },

    // alerts
    success (state, text) {
        state.alert.type = 'success';
        state.alert.text = text;
        state.alert.show = true
    },
    error (state, text) {
        state.alert.type = 'error';
        state.alert.text = text;
        state.alert.show = true
    },
    warning (state, text) {
        state.alert.type = 'warning';
        state.alert.text = text;
        state.alert.show = true
    },
    info (state, text) {
        state.alert.type = 'info';
        state.alert.text = text;
        state.alert.show = true
    },
    closeAlertBox (state) {
        state.alert.show = false
    },

    // auth
    auth_request (state) {
        state.authStatus = 'loading'
    },
    auth_success (state, { token, user, authorization }) {
        state.authStatus = 'success';
        state.token = token;
        state.user = user;

        if (authorization === 'ROLE_ADMIN') {
            localStorage.setItem('adminMode', 'true');
            localStorage.setItem('userMode', 'false')
        }
        else if (authorization === 'ROLE_USER'){
            localStorage.setItem('userMode', 'true');
            localStorage.setItem('adminMode', 'false')
        }
    },
    auth_error (state) {
        state.authStatus = 'error'
    },
    logout (state) {
        state.authStatus = '';
        state.token = ''
    },
    setSignupDialog (state, val) {
        state.signupDialog = val
    },
}
