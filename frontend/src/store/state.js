// https://vuex.vuejs.org/en/state.html

export default {
    dataList: [],
    userIdList: [],
    clientNameList: [],
    productNameList: [],
    userInfo: [],
    item: {},
    secondItem: {},
    user: {},
    loading: true,

    alert: {
        show: false,
        color: '',
        text: '',
        icon: '',
    },

    // Admin
    requestProcess: '',
    createDialog: false,
    editDialog: false,
    isCreating: false,
    isEditting: false,
    isShowLicense: false,
    deleteMode: false,
    licenseMode: false,
    adminPasswordDialog: false,


    // User
    requestDialog: false,
    isRequesting: false,
    isShowRequesting: false,
    isRequestProcessing: false,
    isShowUserLicenseHistory: false,
    isShowUserLicenseList: false,
    showRequestDialog: false,
    showLicenseDialog: false,
    requestProcessDialog: false,
    showUserLicenseHistoryDialog: false,
    showUserLicenseListDialog: false,
    userPasswordDialog: false,
    resetPasswordDialog: false,

    // Auth
    signupDialog: false,
    adminMode: localStorage.getItem('adminMode') || '',
    userMode: localStorage.getItem('userMode') || '',
    authorization: '',
    authStatus: '',
    authRole: '',
    token: localStorage.getItem('token') || '',
}

