/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
import store from '../store'

export default [
  {
    path: '*',
    meta: {
      name: '',
      requiresAuth: true,
      authorize: []
    },
    redirect: {
      path: '/home'
    }
  },
  // This  allows you to have pages apart of the app but no rendered inside the dash
  {
    path: '/',
    meta: {
      name: 'Login',
      requiresAuth: false,
      authorize: []
    },
    component: () =>
        import(/* webpackChunkName: "routes" */ `@/views/auth/Login.vue`),
    // redirect if already signed in
    beforeEnter: (to, from, next) => {
      if (store.getters.authorized) {
        next('/home')
      } else {
        next()
      }
    },
    children: [
      {
        path: '',
        component: () => import(`@/components/LoginForm.vue`)
      }
    ]

  },
  // add any extra routes that you want rendered in the dashboard as a child below. Change toolbar names here
  {
    path: '/home',
    meta: {
      requiresAuth: true,
      authorize: ['ROLE_ADMIN', 'ROLE_USER']
    },
    component: () => import(`@/views/BoardView.vue`),
    children: [
      {
        path: '',
        name: 'Home',
        meta: {
          authorize: ['ROLE_ADMIN', 'ROLE_USER']
        },
        component: () => import(`@/views/Home.vue`)
      },
    ]
  },
    // 관리자 화면
    // 라이센스
  {
    path: '/license',
    meta: {
      requiresAuth: true,
      authorize: ['ROLE_ADMIN']
    },
    component: () => import(`@/views/BoardView.vue`),
    children: [
      {
        path: 'management',
        name: '라이센스 전체 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/license/Licenses.vue`)
      },
      {
        path: 'unapprove',
        name: '라이센스 미승인 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/license/unapprovedRequests.vue`)
      },
      {
        path: 'approve',
        name: '라이센스 승인 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/license/approvedRequests.vue`)
      },
      {
        path: 'reject',
        name: '라이센스 반려 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/license/rejectedRequests.vue`)
      }
    ]
  },
    // 사용자
  {
    path: '/user',
    meta: {
      requiresAuth: true,
      authorize: ['ROLE_ADMIN']
    },
    component: () => import(`@/views/BoardView.vue`),
    children: [
      {
        path: 'management',
        name: '사용자 전체 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/user/Users.vue`)
      },
      {
        path: 'unapprove',
        name: '사용자 미승인 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/user/unapprovedRequests.vue`)
      },
      {
        path: 'approve',
        name: '사용자 승인 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/user/approvedRequests.vue`)
      },
      {
        path: 'reject',
        name: '사용자 반려 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/user/rejectedRequests.vue`)
      }
    ]
  },
    // 고객
  {
    path: '/client',
    meta: {
      requiresAuth: true,
      authorize: ['ROLE_ADMIN']
    },
    component: () => import(`@/views/BoardView.vue`),
    children: [
      {
        path: 'management',
        name: '고객 전체 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/client/Clients.vue`)
      },
    ]
  },
    // 제품
  {
    path: '/product',
    meta: {
      requiresAuth: true,
      authorize: ['ROLE_ADMIN']
    },
    component: () => import(`@/views/BoardView.vue`),
    children: [
      {
        path: 'management',
        name: '제품 전체 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/product/Products.vue`)
      },
    ]
  },
  {
    path: '/adminInfo',
    meta: {
      requiresAuth: true,
      authorize: ['ROLE_ADMIN']
    },
    component: () => import(`@/views/BoardView.vue`),
    children: [
      {
        path: 'info',
        name: '정보 변경 ',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/info/AdminInfo.vue`)
      },
      {
        path: 'passwd',
        name: '비밀번호 변경 ',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN']
        },
        component: () => import(`@/views/admin/info/AdminPassword.vue`)
      }
    ]
  },


    // ============================================== 사용자 화면 ==============================================
  {
    path: '/user',
    meta: {
      requiresAuth: true,
      authorize: ['ROLE_ADMIN', 'ROLE_USER']
    },
    component: () => import(`@/views/BoardView.vue`),
    children: [
      {
        path: 'license',
        name: '라이센스 목록',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN', 'ROLE_USER']
        },
        component: () => import(`@/views/user/UserLicenses.vue`)
      }
    ]
  },
  {
    path: '/userInfo',
    meta: {
      requiresAuth: true,
      authorize: ['ROLE_ADMIN', 'ROLE_USER']
    },
    component: () => import(`@/views/BoardView.vue`),
    children: [
      {
        path: 'info',
        name: '정보 변경',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN', 'ROLE_USER']
        },
        component: () => import(`@/views/user/UserInfo.vue`)
      },
      {
        path: 'passwd',
        name: '비밀번호 변경',
        meta: {
          requiresAuth: true,
          authorize: ['ROLE_ADMIN', 'ROLE_USER']
        },
        component: () => import(`@/views/user/UserPassword.vue`)
      }
    ]
  }
]
