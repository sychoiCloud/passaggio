
/**
 * Vue Router
 *
 * @library
 *
 * https://router.vuejs.org/en/
 */

// Lib imports
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'
import Router from 'vue-router'
import store from '../store'
import Meta from 'vue-meta'
import axios from 'axios'

// Routes
import paths from './paths'

Vue.use(Router);

// Create a new router
const router = new Router({
  mode: 'history',
  routes: paths,

  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return { selector: to.hash }
    }
    return { x: 0, y: 0 }
  }
});

// Route guard checks to see if you are logged in, if not reroutes to login
// to is where you are going, matched.some is to find which routes have requiresAuth
router.beforeEach((to, from, next) => {
  const { authorize } = to.meta;

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.authorized) {
      if (authorize.length && !authorize.includes(localStorage.getItem('role'))) {
        console.log('You dont have authorization...');
        localStorage.clear();
        return next('/')
      }
      else
        next();
      return
    }
    next('/')
  } else {
    next()
  }
});

// const apiRootPath = process.env.NODE_ENV !== 'production' ? 'http://localhost:3000/api/' : '/api/'
// Vue.prototype.$apiRootPath = apiRootPath
axios.defaults.baseURL = 'http://10.10.0.56:8080'
axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    config.headers.Authorization = localStorage.getItem('token');
    return config
}, function (error) {
    // Do something with request error
    return Promise.reject(error)
});

// // Add a response interceptor
// axios.interceptors.response.use(function (response) {
//     // Do something with response data
//     return response
// }, function (error) {
//     // Do something with response error
//     return Promise.reject(error)
// });

Vue.use(Meta);


export default router
