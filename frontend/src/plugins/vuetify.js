import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import theme from './theme'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  theme
});

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  }
});