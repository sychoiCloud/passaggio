import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import vuetify from './plugins/vuetify'
import Vuetify from 'vuetify'
import VuetifyDialog from 'vuetify-dialog'
import VueResource from 'vue-resource'
import VueCookie from 'vue-cookies'
import 'vuetify/dist/vuetify.min.css'
import 'vue-material-design-icons/styles.css'
import Vuelidate from 'vuelidate'
import axios from 'axios'
// Components
import './components'

// Plugins
import './plugins'
import { sync } from 'vuex-router-sync'

// axios.defaults.baseURL = 'http://localhost:8080'

// Sync store with router
sync(store, router);

Vue.config.productionTip = false;
Vue.use(Vuelidate);
Vue.use(Vuetify);
Vue.use(VuetifyDialog);
Vue.use(VueResource);
Vue.use(VueCookie);

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');

