import axios from 'axios'
import { mapState, mapActions } from 'vuex'
export default {
    computed: {
        ...mapState(['showRequestDialog', 'item', 'idList', 'userIdList', 'productNameList', 'clientNameList']),
    },
    props: [
    ],
    methods: {
        ...mapActions(['pressShowRequestButton', 'pushDefaultItem', 'pushRequestProcessing', 'openAlertBox']),

        close () {
            this.pressShowRequestButton(false)
        },

        reject () {
            axios.post(`/api/license/admin/reject`, this.clickedItem)
                .then(response => {
                    this.openAlertBox([
                        'success',
                        '반려 완료',
                    ]);
                    this.pushRequestProcessing(false)
                })
                .catch(error => {
                    this.openAlertBox([
                        'error',
                        error.response.data.message
                    ])
                });
            this.close()
        },

        accept () {
            axios.post(`/api/license/admin/accept`, this.clickedItem)
                .then(response => {
                    this.openAlertBox([
                        'success',
                        '승인 완료',
                    ]);
                    this.pushRequestProcessing(false)
                })
                .catch(error => {
                this.openAlertBox([
                    'error',
                    error.response.data.message
                ])
            });

            this.close()
        }
    }
}