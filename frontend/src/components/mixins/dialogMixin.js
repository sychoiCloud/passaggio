import axios from 'axios'
import { mapState, mapActions } from 'vuex'
export default {
    computed: {
        ...mapState(['item'])
    },
    props: [
    ],
    methods: {
        ...mapActions(['pressCreateButton', 'pressEditButton', 'setClickedItem', 'pushDefaultItem', 'pushCreating', 'pushEditting', 'openAlertBox']),

        createClose () {
            this.pressCreateButton(false);
            setTimeout(() => {
                this.createItem = Object.assign({}, this.defaultItem)
            }, 100)
        },

        editClose () {
            this.pressEditButton(false);
            setTimeout(() => {
                this.clickedItem = Object.assign({}, this.defaultItem)
            }, 100)
        },

        createSave (table, selectedName, refTable, refNum) {
            let obj = {}

            if (refNum == 0) {
                axios.post(`/api/${ table }`, this.createItem)
                    .then(() => {
                        this.openAlertBox([
                            'success',
                            '생성 완료',
                        ]);
                        this.pushCreating(false)
                    })
                    .catch(error => {
                        console.log(error.response);
                        this.openAlertBox([
                            'error',
                            error.response.data.message
                        ])
                    });
            }
            else if (refNum == 2) {

            }

            this.createClose()
        },

        editSave (table, selectedName, refNum) {
            let obj = {};
            if (refNum === 0) {
                axios.put(`/api/${ table }/admin/${ this.clickedItem.id }`, this.clickedItem)
                    .then(() => {
                        this.openAlertBox([
                            'success',
                            '수정 완료',
                        ]);
                        this.pushEditting(false)
                    })
                    .catch(error => {
                        console.log(error.response);
                        this.openAlertBox([
                            'error',
                            error.response.data.message
                        ])
                    });

            }
            else if (refNum === 2) {
                axios.get(`/api/${ table }/${ selectedName[0] }:${ selectedName[1] }:${ selectedName[2] }:${ selectedName[3] }`)
                    .then(response => {
                        obj = response.data;
                        obj.code = this.clickedItem.code;
                        obj.type = this.clickedItem.type;
                        obj.regDate = this.clickedItem.regDate;
                        obj.expDate = this.clickedItem.expDate;
                        obj.licenseKey = this.clickedItem.licenseKey;

                        axios.put(`/api/${ table }/admin/${ obj.id }`, obj)
                            .then(() => {
                                this.openAlertBox([
                                    'success',
                                    '수정 완료'
                                ]);
                                this.pushEditting(false)
                            })
                            .catch(error => {
                                this.openAlertBox([
                                    'error',
                                    error.response.data.message
                                ])
                            });

                    })
                    .catch(error => {
                        console.log(error.response);
                        this.openAlertBox([
                            'error',
                            error.response.data.message
                        ])
                    });
            }

            this.editClose()
        },
    }
}