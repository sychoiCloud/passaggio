FROM java:8
VOLUME ./ ADD /target/springboot_csy-0.0.1-SNAPSHOT.jar springboot_image.jar
ENTRYPOINT ["java","-jar","springboot_image.jar"]
