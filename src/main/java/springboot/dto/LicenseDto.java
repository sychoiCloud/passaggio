package springboot.dto;

import lombok.*;
import springboot.model.Client;
import springboot.model.License;
import springboot.model.Product;
import springboot.model.User;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class LicenseDto {

    private Long id;
    private String licenseKey;
    private String regDate;
    private String modDate;
    private String stDate;
    private String expDate;
    private int hostCount;
    private String processStatus;
    private String projectName;
    private String projectAdminName;
    private String projectAdminPhone;
    private String projectAdminEmail;
    private String type;
    private String flag;
    private String status;
    private byte[] content;
    private String contentRead;
    private String request;
    private String model;
    private String partNo;
    private String productVersion;

    private Client client;
    private User user;
    private Product product;

    public License toEntity() {
        return License.builder()
                .id(id)
                .licenseKey(licenseKey)
                .regDate(regDate)
                .modDate(modDate)
                .stDate(stDate)
                .expDate(expDate)
                .hostCount(hostCount)
                .processStatus(processStatus)
                .projectName(projectName)
                .projectAdminName(projectAdminName)
                .projectAdminPhone(projectAdminPhone)
                .projectAdminEmail(projectAdminEmail)
                .type(type)
                .flag(flag)
                .status(status)
                .content(content)
                .contentRead(contentRead)
                .request(request)
                .model(model)
                .partNo(partNo)
                .productVersion(productVersion)
                .client(client)
                .user(user)
                .product(product)
                .build();
    }

}