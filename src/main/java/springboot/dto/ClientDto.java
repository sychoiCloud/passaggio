package springboot.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import springboot.model.Client;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ClientDto {

    private Long id;
    private String code;
    private String name;
    private String ename;
    private String phone;
    private String email;
    private String category;
    private String flag;
    private String nameNospace;
    private String regDate;
    private String modDate;

    public Client toEntity() {
        return Client.builder()
                .id(id)
                .code(code)
                .name(name)
                .ename(ename)
                .phone(phone)
                .email(email)
                .category(category)
                .flag(flag)
                .nameNospace(nameNospace)
                .regDate(regDate)
                .modDate(modDate)
                .build();
    }
}