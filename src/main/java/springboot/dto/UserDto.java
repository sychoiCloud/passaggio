package springboot.dto;

import lombok.*;
import springboot.model.Role;
import springboot.model.User;

import java.util.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserDto {

    private String id;

    private String password;
    private String username;
    private String phone;
    private String email;
    private String regDate;
    private String modDate;
    private String status;
    private String processStatus;
    private String token;
    private String flag;
    private String request;
    private String authority;
    private Set<Role> roles = new HashSet<>();


    public User toEntity() {
        return User.builder()
                .id(id)
                .password(password)
                .username(username)
                .phone(phone)
                .email(email)
                .regDate(regDate)
                .modDate(modDate)
                .status(status)
                .processStatus(processStatus)
//                .token(token)
                .flag(flag)
                .request(request)
                .authority(authority)
                .build();
    }
}