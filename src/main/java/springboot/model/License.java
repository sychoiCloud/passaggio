package springboot.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "license")
public class License implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name = "license_key")
    private String licenseKey;
    @Column(name = "reg_date")
    private String regDate;
    @Column(name = "mod_date")
    private String modDate;
    @Column(name = "st_date")
    private String stDate;
    @Column(name = "exp_date")
    private String expDate;
    @Column(name = "host_count")
    private int hostCount;
    @Column(name = "process_status")
    private String processStatus;
    @Column(name = "project_name")
    private String projectName;
    @Column(name = "project_admin_name")
    private String projectAdminName;
    @Column(name = "project_admin_phone")
    private String projectAdminPhone;
    @Column(name = "project_admin_email")
    private String projectAdminEmail;
    @Column(name = "product_version")
    private String productVersion;

    private String type;
    private String flag;
    private String status;
    @Lob
    private byte[] content;
    private String contentRead;
    private String request;
    private String model;
    @Column(name = "part_no")
    private String partNo;

//    @JsonBackReference
    @ManyToOne
    private Client client;

    @ManyToOne
    private User user;

    @ManyToOne
    private Product product;

    @Builder
    public License(Long id, String licenseKey, String regDate, String modDate, String stDate, String expDate, int hostCount, String processStatus, String projectName,
                   String projectAdminName, String projectAdminPhone, String projectAdminEmail, String type, String flag, String status, byte[] content,
                   String contentRead, String request, String model, String partNo, String productVersion, Client client, User user, Product product) {
        this.id = id;
        this.licenseKey = licenseKey;
        this.regDate = regDate;
        this.modDate = modDate;
        this.stDate = stDate;
        this.expDate = expDate;
        this.hostCount = hostCount;
        this.processStatus = processStatus;
        this.projectName = projectName;
        this.projectAdminName = projectAdminName;
        this.projectAdminPhone = projectAdminPhone;
        this.projectAdminEmail = projectAdminEmail;
        this.type = type;
        this.flag = flag;
        this.status = status;
        this.content = content;
        this.contentRead = contentRead;
        this.request = request;
        this.model = model;
        this.partNo = partNo;
        this.productVersion = productVersion;
        this.client = client;
        this.user = user;
        this.product = product;
    }

    public License() {}
}