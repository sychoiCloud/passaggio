package springboot.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "user")
public class User implements Serializable {

    @Id
    @Column(name="id")
    private String id;

    @Column(name = "password")
    @Size(max = 110)
    private String password;

    @Column(name = "username")
    @Size(max = 20)
    private String username;

    @Column(name = "phone")
    @Size(max = 13)
    private String phone;

    @Column(name = "email")
    @Size(max = 50)
    private String email;

    @Column(name = "reg_date")
    private String regDate;

    @Column(name = "mod_date")
    private String modDate;

    @Column(name = "process_status")
    private String processStatus;

    private String status;
    private String token;
    private String flag;
    private String request;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    private String authority;

    @Builder
    public User(String id, String password, String username, String phone, String email, String regDate, String modDate,
                String status, String processStatus, String flag, String request, String authority, String token, Set<Role> roles) {
        this.id = id;
        this.password = password;
        this.username = username;
        this.phone = phone;
        this.email = email;
        this.regDate = regDate;
        this.modDate = modDate;
        this.status = status;
        this.processStatus = processStatus;
        this.token = token;
        this.flag = flag;
        this.request = request;
        this.authority = authority;
        this.roles = roles;
    }

    public User() {};
}