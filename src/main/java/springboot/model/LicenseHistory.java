package springboot.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Table(name = "license_history")
@NoArgsConstructor
public class LicenseHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name = "license_id")
    private Long licenseId;
    @Column(name = "license_key")
    private String licenseKey;
    @Column(name = "req_date")
    private String reqDate;
    @Column(name = "reg_date")
    private String regDate;
    @Column(name = "st_date")
    private String stDate;
    @Column(name = "exp_date")
    private String expDate;
    @Column(name = "process_status")
    private String processStatus;
    @Column(name = "project_name")
    private String projectName;
    @Column(name = "project_admin_name")
    private String projectAdminName;
    @Column(name = "project_admin_phone")
    private String projectAdminPhone;
    @Column(name = "project_admin_email")
    private String projectAdminEmail;
    @Column(name = "host_count")
    private Integer hostCount;
    @Column(name = "user_id")
    private String userId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "client_id")
    private Long clientId;
    @Column(name = "client_code")
    private String clientCode;
    @Column(name = "client_name")
    private String clientName;
    @Column(name = "product_name")
    private String productName;
    @Column(name = "product_version")
    private String productVersion;
    private String model;
    @Column(name = "part_no")
    private String partNo;
    private String type;
    private String flag;
    private String status;

    public LicenseHistory(License li, String todayStr) {
        this.licenseId = li.getId();
        this.licenseKey = li.getLicenseKey();
        this.reqDate = li.getModDate();             // 라이센스 요청일시(가장 최근 변경된 일시)
        this.regDate = todayStr;                    // 라이센스 요청처리일시
        this.stDate = li.getStDate();             // 라이센스 시작일자
        this.expDate = li.getExpDate();             // 라이센스 만료일자
        this.type = li.getType();
        this.flag = li.getFlag();
        this.status = li.getStatus();
        this.processStatus = li.getProcessStatus();
        this.projectName = li.getProjectName();
        this.projectAdminName = li.getProjectAdminName();
        this.projectAdminPhone = li.getProjectAdminPhone();
        this.projectAdminEmail = li.getProjectAdminEmail();
        this.hostCount = li.getHostCount();
        this.model = li.getModel();
        this.partNo = li.getPartNo();
        this.userId = li.getUser().getId();
        this.userName = li.getUser().getUsername();
        this.clientId = li.getClient().getId();
        this.clientCode = li.getClient().getCode();
        this.clientName = li.getClient().getName();
        this.productName = li.getProduct().getName();
        this.productVersion = li.getProductVersion();
    }
}