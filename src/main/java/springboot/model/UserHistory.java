package springboot.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString
@Table(name = "user_history")
@NoArgsConstructor
public class UserHistory implements Serializable {

    @Id
    @Column(name="id")
    private String id;

    @Column(name = "username")
    @Size(max = 20)
    private String username;

    @Column(name = "phone")
    @Size(max = 13)
    private String phone;

    @Column(name = "email")
    @Size(max = 50)
    private String email;

    @Column(name = "req_date")
    private String reqDate;
    @Column(name = "reg_date")
    private String regDate;

    private String request;
    private String status;
    private String processStatus;
}