package springboot.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@Table(name = "client")
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String name;
    private String ename;
    private String phone;
    private String email;
    private String category;
    private String flag;

    @Column(name = "name_nospace")
    private String nameNospace;
    @Column(name = "reg_date")
    private String regDate;
    @Column(name = "mod_date")
    private String modDate;

    @Builder
    public Client(Long id, String code, String name, String ename, String phone, String email, String category, String flag, String nameNospace, String regDate, String modDate) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.ename = ename;
        this.phone = phone;
        this.email = email;
        this.category = category;
        this.flag = flag;
        this.nameNospace = nameNospace;
        this.regDate = regDate;
        this.modDate = modDate;
    }

    public Client() {}
}