package springboot.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;
import springboot.dto.UserDto;
import springboot.model.User;
import springboot.model.UserHistory;
import springboot.repository.UserHistoryRepository;
import springboot.repository.UserRepository;
import springboot.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("http://10.10.0.56:8080")
@RequestMapping("api/user")
@AllArgsConstructor
public class UsersController {

    private UserService userService;
    private UserRepository userRepository;

//    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
//    public ResponseEntity<List<User>> getAllUsers() {
//        List<User> User = userRepository.findAll();
//        return new ResponseEntity<List<User>>(User, HttpStatus.OK);
//    }

    @GetMapping(value = "/admin", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<User>> getAllUsersExcludeAdmin() {
        List<User> userList = userService.getAll();
        return new ResponseEntity<List<User>>(userList, HttpStatus.OK);
    }

    // 유저 조회
    // 다른 유저일땐 못얻게 할 필요 있음
    @GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<User> getById(@PathVariable("id") String id) {
        Optional<User> user = userRepository.findById(id);
        return new ResponseEntity<User>(user.get(), HttpStatus.OK);
    }

    @GetMapping(value = "/idList", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<String>> getAllUserIdList() {
        List<String> userIdList = userService.getUserIdList();
        return new ResponseEntity<List<String>>(userIdList, HttpStatus.OK);
    }

    // 처리상태별 리스트 GET
    @GetMapping(value = "/admin/process/{processStatus}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<User>> getUserByProcess(@PathVariable String processStatus) {
        List<User> users = userService.getByProcessStatus(processStatus);
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    // 처리상태별 히스토리 리스트 GET
    @GetMapping(value = "/admin/history/process/{processStatus}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<UserHistory>> getAllRequestProcessMod(@PathVariable String processStatus) {
        List<UserHistory> userHistories = userService.getHistoryByProcessStatus(processStatus);
        return new ResponseEntity<List<UserHistory>>(userHistories, HttpStatus.OK);
    }

    // 비밀번호 확인
    // 다른 유저일땐 못얻게 할 필요 있음
    @PutMapping(value = "/passwd/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> checkPassword(@PathVariable String id, @RequestBody UserDto user) {
        ResponseEntity<?> message = userService.checkPasswd(id, user.toEntity());
        return message;
    }

    // 유저 정보 수정
    @PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody UserDto user) {
        ResponseEntity<?> message = userService.updateById(id, user.toEntity());
        return message;
    }

    // 유저 정보 저장
    @PostMapping(value = "/admin")
    public ResponseEntity<?> save(@RequestBody UserDto user) {
        ResponseEntity<?> message = userService.save(user.toEntity());

        return message;
    }

    // 유저 정보 삭제
    @DeleteMapping(value = "/admin/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> deleteById(@PathVariable String id) {
        ResponseEntity<?> message = userService.deleteById(id);
        return message;
    }

    // 승인
    @PostMapping(value = "/admin/accept")
    public ResponseEntity<?> accept(@RequestBody UserDto user) {
        ResponseEntity<?> message = userService.accept(user.toEntity());
        return message;
    }

    // 반려
    @PostMapping(value = "/admin/reject")
    public ResponseEntity<?> reject(@RequestBody UserDto user) {
        ResponseEntity<?> message = userService.reject(user.toEntity());
        return message;
    }

    // 요청 취소
    @PostMapping(value = "/admin/cancel")
    public ResponseEntity<?> cancel(@RequestBody UserDto user) {
        ResponseEntity<?> message = userService.cancel(user.toEntity());
        return message;
    }
}