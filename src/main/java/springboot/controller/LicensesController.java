package springboot.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import springboot.dto.LicenseDto;
import springboot.model.License;
import springboot.model.LicenseHistory;
import springboot.model.User;
import springboot.repository.LicenseRepository;
import springboot.repository.LicenseHistoryRepository;
import springboot.repository.UserRepository;
import springboot.service.LicenseService;

@RestController
@CrossOrigin("http://10.10.0.56:8080")
@RequestMapping("api/license")
@AllArgsConstructor
public class LicensesController {

    private LicenseService licenseService;
    private LicenseRepository licenseRepository;

    @GetMapping(value = "/admin", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<License>> getAllLicenses() {
        List<License> license = licenseService.getAll();
        return new ResponseEntity<List<License>>(license, HttpStatus.OK);
    }

    @GetMapping(value = "/user/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<License>> getAllLicensesById(@PathVariable("id") String id) {
        List<License> licenses = licenseService.getById(id);
        return new ResponseEntity<List<License>>(licenses, HttpStatus.OK);
    }

    @GetMapping(value = "/admin/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> getLicensesById(@PathVariable("id") Long id) {
        Optional<License> license = licenseRepository.findById(id);
        return new ResponseEntity<License>(license.get(), HttpStatus.OK);
    }

    @GetMapping(value = "/admin/process/{processStatus}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<License>> getLicensesByProcessStatus(@PathVariable("processStatus") String processStatus) {
        List<License> license = licenseService.getByProcessStatus(processStatus);
        return new ResponseEntity<List<License>>(license, HttpStatus.OK);
    }

    @GetMapping(value = "/admin/{id}:{usId}:{pdName}:{clName}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> getByNames(@PathVariable("id") Long id, @PathVariable("usId") String usId, @PathVariable("pdName") String pdName, @PathVariable("clName") String clName) {
        License license = licenseService.getByNames(id, usId, pdName, clName);
        return new ResponseEntity<License>(license, HttpStatus.OK);
    }

    @DeleteMapping(value = "/admin/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> deleteLicense(@PathVariable("id") Long id) {
        licenseService.deleteById(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(value = "/admin/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> updateLicense(@PathVariable("id") Long id, @RequestBody LicenseDto license) {
        licenseService.updateById(id, license.toEntity());
        return new ResponseEntity<License>(license.toEntity(), HttpStatus.OK);
    }

    @PutMapping(value = "/update", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> updateRequestLicense(@RequestBody LicenseDto license) {
        licenseService.updateRequestById(license.toEntity());
        return new ResponseEntity<License>(license.toEntity(), HttpStatus.OK);
    }

    @PostMapping(value= "/admin")
    public ResponseEntity<License> save(@RequestBody LicenseDto license) {
        return new ResponseEntity<License>(licenseRepository.save(license.toEntity()), HttpStatus.OK);
    }

    @PostMapping(value = "/issue", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> saveIssue(@RequestBody LicenseDto license) {
        return new ResponseEntity<License>(licenseService.saveIssue(license.toEntity()), HttpStatus.OK);
    }

    @PostMapping(value = "/admin/issue", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> saveAdminIssue(@RequestBody LicenseDto license) throws IOException, ParseException {
        return new ResponseEntity<License>(licenseService.saveAdminIssue(license.toEntity()), HttpStatus.OK);
    }


    // =============================================================== 요청 컨트롤러 =============================================================== //
    @Autowired
    private LicenseHistoryRepository licenseHistoryRepository;

    // 히스토리 리스트 GET
    @GetMapping(value = "/admin/history/all", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<LicenseHistory>> getAllLicenseHistory() {
        List<LicenseHistory> licenseRequestResult = licenseHistoryRepository.findAll();
        return new ResponseEntity<List<LicenseHistory>>(licenseRequestResult, HttpStatus.OK);
    }

    // 히스토리 ID로 GET
    // 다른 유저는 검색 못하게 막아야함
    @GetMapping(value = "/history/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<LicenseHistory> getLicenseHistoryById(@PathVariable Long id) {
        Optional<LicenseHistory> licenseHistory = licenseHistoryRepository.findById(id);
        return new ResponseEntity<LicenseHistory>(licenseHistory.get(), HttpStatus.OK);
    }

    // 히스토리 리스트 처리상태별 GET
    @GetMapping(value = "/admin/history/process/{process}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<LicenseHistory>> getLicenseHistoryByProcess(@PathVariable String process) {
        List<LicenseHistory> licenseHistory = licenseService.getHistoryByProcessStatus(process);
        return new ResponseEntity<List<LicenseHistory>>(licenseHistory, HttpStatus.OK);
    }

    // 히스토리 리스트 라이센스별 GET
    @GetMapping(value = "/history/lic/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<LicenseHistory>> getLicenseHistoryByLicenseId(@PathVariable Long id) {
        List<LicenseHistory> licenseHistory = licenseHistoryRepository.findLicenseRequestByLicenseId(id);
        return new ResponseEntity<List<LicenseHistory>>(licenseHistory, HttpStatus.OK);
    }

    // 라이센스 요청 승인
    @PostMapping(value = "/admin/accept", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> accpet(@RequestBody LicenseDto license) throws IOException, ParseException {
        return new ResponseEntity<License>(licenseService.accept(license.toEntity()), HttpStatus.OK);
    }

    // 라이센스 요청 반려
    @PostMapping(value = "/admin/reject", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> reject(@RequestBody LicenseDto license) throws IOException {
        return new ResponseEntity<License>(licenseService.reject(license.toEntity()), HttpStatus.OK);
    }

    // 라이센스 요청 취소
    @PutMapping(value = "/cancel", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<License> cancel(@RequestBody LicenseDto license) {
        return new ResponseEntity<License>(licenseService.cancel(license.toEntity()), HttpStatus.OK);
    }

    // 라이센스 파일 다운로드
    @GetMapping("/file/{id}")
    public ResponseEntity<InputStreamResource> getFile(@PathVariable Long id) throws IOException {
        Optional<License> license = licenseRepository.findById(id);

        if(license.isPresent()) {
//			MediaType mediaType = MediaType.parseMediaType("application/vnd.ms-excel");
            Charset utf8 = Charset.forName("UTF-8");
            MediaType mediaType = new MediaType("application", "json", utf8);
            File file = new File("temp.lic");
            FileUtils.writeByteArrayToFile(file, license.get().getContent());
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

            return ResponseEntity.ok()
                    // Content-Disposition
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                    // Content-Type
                    .contentType(mediaType)
                    // Contet-Length
                    .contentLength(file.length()) //
                    .body(resource);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @GetMapping("/check")
    public void check() throws IOException, ParseException {
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String todayStr = sdf.format(today);
        licenseService.checkLicenseStartDate(todayStr);
        licenseService.checkLicenseExpDate(todayStr);
    }
}