package springboot.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import springboot.model.Role;
import springboot.repository.UserRepository;
import springboot.service.UserService;
import springboot.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

@RestController
@AllArgsConstructor
public class TokenController {

    private UserService userService;
    private UserRepository userRepository;

    @PostMapping(value = "/token", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getToken(@RequestParam("id") String id, @RequestParam("password") String password) {
       String token = userService.login(id, password);
       Optional<User> auth = userRepository.findById(id);
       HashMap<String, String> dataSet = new HashMap<>();
       Set<Role> roles = auth.get().getRoles();
       Iterator<Role> iter = roles.iterator();
       Role role = new Role();
       while (iter.hasNext()) {
           role = iter.next();
       }
       auth.get().setAuthority(role.getName().name());

       dataSet.put("token", token);
       dataSet.put("username", id);
       dataSet.put("authorization", role.getName().name());

       if (StringUtils.isEmpty(token)){
           return new ResponseEntity<>("not Found", HttpStatus.NOT_FOUND);
       }
        return new ResponseEntity<>(dataSet, HttpStatus.OK);
    }

}
