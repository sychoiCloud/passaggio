package springboot.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.dto.UserDto;
import springboot.model.User;
import springboot.service.UserService;

@RestController
@CrossOrigin("http://10.10.0.56:8080")
@RequestMapping("signup")
@AllArgsConstructor
public class SignupController {

    private UserService userService;

    // 유저 정보 저장
    @PostMapping
    public ResponseEntity<?> save(@RequestBody UserDto user) {
        ResponseEntity<?> message = userService.signup(user.toEntity());
        return message;
    }

    // ID 중복 검사
    @GetMapping(value = "/dupCheck/{id}")
    public ResponseEntity<?> duplicationCheck(@PathVariable String id) {
        ResponseEntity<?> message = userService.dupCheck(id);
        return message;
    }
}