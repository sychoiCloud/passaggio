package springboot.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.dto.ClientDto;
import springboot.model.Client;
import springboot.repository.ClientRepository;
import springboot.response.MessageResponse;
import springboot.service.ClientService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("http://10.10.0.56:8080")
@RequestMapping("api/client")
@AllArgsConstructor
public class ClientsController {

    private ClientService clientService;
    private ClientRepository clientRepository;

    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<Client>> getAllClients() {
        List<Client> clients = clientRepository.findAllByOrderByRegDateDesc();
        return new ResponseEntity<List<Client>>(clients, HttpStatus.OK);
    }

    @DeleteMapping(value = "/admin/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> deleteClient(@PathVariable("id") Long id) {
        clientService.deleteById(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(value = "/admin/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Client> updateClient(@PathVariable("id") Long id, @RequestBody ClientDto client) {
        clientService.updateById(id, client.toEntity());
        return new ResponseEntity<Client>(client.toEntity(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Client> save(@RequestBody ClientDto client) {
        return new ResponseEntity<Client>(clientService.save(client.toEntity()), HttpStatus.OK);
    }

    @GetMapping(value = "/nameList", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<String>> getAllClientNameList() {
        List<String> nameList = clientService.getNameList();
        return new ResponseEntity<List<String>>(nameList, HttpStatus.OK);
    }

    @GetMapping(value = "/dupCheck/{name}")
    public ResponseEntity<?> duplicationCheck(@PathVariable String name) {
        ResponseEntity<?> message = clientService.dupCheck(name);
        return message;

    }




}