package springboot.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.service.EmailService;


@RestController
@CrossOrigin("http://10.10.0.56:8080")
@RequestMapping("email")
@AllArgsConstructor
public class EmailController {

    EmailService emailService;

    @PostMapping(value = "/send/{id}/{email}")
    public ResponseEntity<?> sendEmail(@PathVariable String id, @PathVariable String email) {
        ResponseEntity<?> message = emailService.sendPasswordResetMessage(email, id,"패스워드 초기화 알림");
        return message;
    }
}
