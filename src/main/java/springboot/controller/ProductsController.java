package springboot.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.model.Product;
import springboot.repository.ProductRepository;
import springboot.service.ProductService;

import java.util.List;

@RestController
@CrossOrigin("http://10.10.0.56:8080")
@RequestMapping("api/product")
@AllArgsConstructor
public class ProductsController {

    private ProductService productService;
    private ProductRepository productRepository;

    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> product = productService.findAll();
        return new ResponseEntity<List<Product>>(product, HttpStatus.OK);
    }

    @GetMapping(value = "/{name}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Product> getByProductName(@PathVariable("name") String name) {
        Product product = productService.getByName(name);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @GetMapping(value = "/nameList", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<String>> getAllProductNameList() {
        List<String> nameList = productService.getNameList();
        return new ResponseEntity<List<String>>(nameList, HttpStatus.OK);
    }

    @PutMapping(value = "/admin/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> updatProduct(@PathVariable("id") Long id, @RequestBody Product product) {
        ResponseEntity<?> message = productService.updateById(id, product);

        return message;
    }

    @DeleteMapping(value = "/admin/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id) {
        ResponseEntity<?> message = productService.deleteById(id);

        return message;
    }

    @PostMapping(value = "/admin")
    public ResponseEntity<Product> save(@RequestBody Product product) {
        return new ResponseEntity<Product>(productRepository.save(product), HttpStatus.OK);
    }
}