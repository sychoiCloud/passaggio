package springboot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringbootCsyApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCsyApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}