package springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    List<User> findAllByOrderByRegDateDesc();
//    Optional<User> findById(String id);
    User findByIdAndPassword(String id, String password);
    User findByToken(String token);
    List<User> findAllByProcessStatusOrderByRegDateDesc(String process);

    Boolean existsByEmail(String id);
    Boolean existsByPhone(String id);

    User findByIdAndEmail(String id, String Email);
}