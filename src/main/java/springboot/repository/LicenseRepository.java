package springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import springboot.model.License;
import springboot.model.User;

import java.util.List;

@Repository
public interface LicenseRepository extends JpaRepository<License, Long> {
    List<License> findAllByUserOrderByRegDateDesc(User user);
    List<License> findAllByOrderByRegDateDesc();
    List<License> findAllByProcessStatusOrderByRegDateDesc(String processStatus);

    License save(License license);
}