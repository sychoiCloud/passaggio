package springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.model.Client;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByName(String name);
    void deleteById(Long id);
    List<Client> findAllByOrderByRegDateDesc();
    Client findByNameNospace(String name);
}