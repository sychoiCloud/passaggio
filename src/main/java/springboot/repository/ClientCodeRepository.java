package springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.model.ClientCode;

@Repository
public interface ClientCodeRepository extends JpaRepository<ClientCode, Long> {
    ClientCode findByCategory(String code);
}