package springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.model.User;
import springboot.model.UserHistory;

import java.util.List;

@Repository
public interface UserHistoryRepository extends JpaRepository<UserHistory, String> {
    List<UserHistory> findAllByProcessStatusOrderByRegDateDesc(String process);
}