package springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.model.LicenseHistory;

import java.util.List;

@Repository
public interface LicenseHistoryRepository extends JpaRepository<LicenseHistory, Long> {
    List<LicenseHistory> findLicenseRequestByLicenseId(Long id);
    List<LicenseHistory> findAllByProcessStatusOrderByRegDateDesc(String process);
}