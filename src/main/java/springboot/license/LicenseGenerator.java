package springboot.license;

import springboot.license.utils.DateUtils;
import springboot.license.licensor.ExtLicense;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

public class LicenseGenerator {

    public void check(String secret_key, File licFile) {
        ExtLicense extLic = new ExtLicense(secret_key.getBytes());
        try {
            extLic.setLicense(licFile);
            System.out.println("expired : " + extLic.isExpired());
            System.out.println("verified : " + extLic.verifying());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public void generate(File licFile, String secret_key, String periodType, String expired, String customerCode, String product, String model, String partNo, int hostCount, String version) {
        Date date = DateUtils.strToDate(expired);
        if (date == null) {
            System.out.println("입력한 날짜가 잘못되었습니다");
            return;
        }

        try {
            ExtLicense extLic = new ExtLicense(secret_key.getBytes());

            extLic.generateLicenseId();

            extLic.setCustomerCode(customerCode);

            extLic.setPeriodType(periodType);

            extLic.setExpiry(date);

            extLic.setHostCount(hostCount);

            extLic.setProduct(product);
            extLic.setProductVersion(version);
            extLic.setModel(model);
            extLic.setPartNo(partNo);

            extLic.signature();

            extLic.dumpLicense(licFile);

            System.out.println(extLic.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
