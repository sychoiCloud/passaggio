package springboot.license.licensor;

import com.verhas.licensor.License;
import springboot.license.mac.HmacAtuh;
import springboot.license.properties.SortedProperties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * ExtendedLicense supports not only String features, but also Integer, Date and
 * URL features. It is also able to check that the license is not expired and
 * can check the revocation state of the license online.
 *
 * @author Peter Verhas
 *
 */
public class ExtLicense extends License {

    final private static String DATE_FORMAT = "yyyy-MM-dd";
    final private static String CUSTOMER_CODE = "custom_code";
    final private static String PERIOD_TYPE = "period_type";
    final private static String EXPIRATION_DATE = "expiry_date";
    final private static String HOST_COUNT = "host_count";
    final private static String LICENSE_ID = "license_id";
    final private static String PRODUCT_NAME = "product_name";
    final private static String PRODUCT_VERSION = "product_version";
    final private static String MODEL = "model";
    final private static String PART_NO = "part_no";
    final private static String SIGNATURE = "signature";



    private boolean verified = false;
    private byte[] secretKey = null;


    public ExtLicense(byte[] secretKey){
        setSecretKey(secretKey);
    }

    private void setSecretKey(final byte[] secretKey) {
        this.secretKey = secretKey;
    }

    /**
     * Checks the expiration date of the license and returns true if the license
     * has expired.
     * <p>
     * The expiration date is encoded in the license with the key
     * {@code expiryDate} in the format {@code yyyy-MM-dd}. A license is expired
     * if the current date is after the specified expiryDate. At the given date
     * the license is still valid.
     * <p>
     * Note that this method does not ensure license validity. You separately
     * have to call {@link License#isVerified()} to ensure that the license was
     * successfully verified.
     * <p>
     * The time is calculated using the default locale, thus licenses expire
     * first in Australia, later in Europe and latest in USA.
     *
     * @return {@code true} if the license is expired
     *
     * @throws ParseException if the date is badly formatted
     */
    public boolean isExpired() throws ParseException {
        boolean expired;
        Date expiryDate;
        try {
            expiryDate = getFeature(EXPIRATION_DATE, Date.class);
            final GregorianCalendar today = new GregorianCalendar();
//            SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            Date date = new Date();
//            transFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
//
//            String todayStr = transFormat.format(date);
            today.set(Calendar.HOUR, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.SECOND, 0);
            today.set(Calendar.MILLISECOND, 0);
            expired = today.getTime().after(expiryDate);
        } catch (final Exception e) {
            expired = true;
        }
        return expired;
    }

    public void setHostCount(final int hostCount) {
        setFeature(HOST_COUNT, String.valueOf(hostCount));
    }


    /**
     * Set the expiration date of the license. Since the date is stored in the
     * format {@code yyyy-MM-dd} the actual hours, minutes and so on will be
     * chopped off.
     */
    public void setExpiry(final Date expiryDate) {
        setFeature(EXPIRATION_DATE, expiryDate);
    }

    /**
     * Generates a new license id.
     * <p>
     * Note that this ID is also stored in the license thus there is no need to
     * call {@link #setFeature(String, UUID)} separately after the UUID was
     * generated.
     * <p>
     * Generating UUID can be handy when you want to identify each license
     * individually. For example you want to store revocation information about
     * each license. The url to check the revocation may contain the
     * <tt>${licenseId}</tt> place holder that will be replaced by the actual
     * uuid stored in the license.
     *
     * @return the generated uuid.
     */
    public String generateLicenseId() {
        final String licenseId = UUID.randomUUID().toString().substring(4, 28);
        setLicenseId(licenseId);
        return licenseId;
    }

    /**
     * Set the UUID of a license. Note that this UUID can be generated calling
     * the method {@link #generateLicenseId()}, which method automatically calls
     * this method setting the generated UUID to be the UUID of the license.
     *
     * @param licenseId
     *            the uuid that was generated somewhere, presumably not by
     *            {@link #generateLicenseId()} because the uuid generated by
     *            that method is automatically stored in the license.
     */
    public void setLicenseId(final String licenseId) {
        setFeature(LICENSE_ID, licenseId);
    }

    public String getLicenseId() {
        return getFeature(LICENSE_ID);
    }

    /**
     * Set a Product Name in the license.
     *
     * @param product
     */
    public void setProduct(final String product) {
        setFeature(PRODUCT_NAME, product);
    }

    public String geProduct() {
        return getFeature(PRODUCT_NAME);
    }

    /**
     * Set a Product Version in the license.
     *
     * @param version
     */
    public void setProductVersion(final String version) {
        setFeature(PRODUCT_VERSION, version);
    }

    public String getProductVersion() {
        return getFeature(PRODUCT_VERSION);
    }

    /**
     * Set a Customer Code in the license.
     *
     * @param customerCode
     */
    public void setCustomerCode(final String customerCode) {
        setFeature(CUSTOMER_CODE, customerCode);
    }

    public String getCustomerCode() {
        return getFeature(CUSTOMER_CODE);
    }

    /**
     * Set a Period Type in the license.
     *
     * @param periodType
     */
    public void setPeriodType(final String periodType) {
        if (periodType.equals("기간")) {
            setFeature(PERIOD_TYPE, "subscription");
        }
        else if(periodType.equals("영구")){
            setFeature(PERIOD_TYPE, "perpetual");
        }
    }

    public String getPeriodType() {
        return getFeature(PERIOD_TYPE);
    }

    /**
     * Set a Model in the license.
     *
     * @param model
     */
    public void setModel(final String model) {
        setFeature(MODEL, model);
    }

    public String getModel() {
        return getFeature(MODEL);
    }

    /**
     * Set a Part No in the license.
     *
     * @param partNo
     */
    public void setPartNo(final String partNo) {
        setFeature(PART_NO, partNo);
    }

    public String getPartNo() {
        return getFeature(PART_NO);
    }

    public void signature(){
        String signature = HmacAtuh.getSignature(secretKey, getLicenseData());
        setFeature(SIGNATURE, signature);
    }

    public boolean verifying() throws RuntimeException, ParseException {
        String data = HmacAtuh.getSignature(secretKey, getLicenseData());
        String signature = getFeature(SIGNATURE);


        try{
            if(HmacAtuh.verifySignature(secretKey, getLicenseData(), signature)){
                if(!isExpired()){
                    return true;
                }else{
                    return false;
                }
            }
        } catch(RuntimeException e){
            throw e;
        } catch (ParseException e) {
            throw e;
        }

        return verified;
    }

    private String getLicenseData() {
        StringBuffer sb = new StringBuffer();

        try {
            sb.append(getFeature(LICENSE_ID, String.class));
            sb.append(getFeature(CUSTOMER_CODE, String.class));
            sb.append(getFeature(PRODUCT_NAME, String.class));
            sb.append(getFeature(EXPIRATION_DATE, String.class));
            sb.append(getFeature(HOST_COUNT, String.class));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    @Override
    public void dumpLicense(final File file) throws IOException {
        Calendar calendar = new GregorianCalendar(Locale.KOREA);
        int nYear = calendar.get(Calendar.YEAR);
        String year = Integer.toString(nYear);
        String comments = "Copyright " + year + " Okestro All Right Reserved.";
        dumpLicense(new FileOutputStream(file), comments);
    }

    private void dumpLicense(final OutputStream os, String comments) throws IOException {
        if (super.licenseProperties != null) {
            SortedProperties sp = new SortedProperties();
            sp.putAll(super.licenseProperties);
            sp.store(os, comments);
        }
    }

    /**
     * Set an integer feature in the license.
     *
     * @param name
     *            the name of the feature
     * @param i
     *            the value of the integer feature
     */
    public void setFeature(final String name, final Integer i) {
        setFeature(name, i.toString());
    }

    /**
     * Set a date feature in the license.
     *
     * @param name
     *            the name of the feature
     * @param date
     *            the date to be stored for the feature name in the license
     */
    public void setFeature(final String name, final Date date) {

        final DateFormat formatter = new SimpleDateFormat(DATE_FORMAT, Locale.KOREA);
//        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        Date date = new Date();
//        transFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
//
//        String todayStr = transFormat.format(date);
        setFeature(name, formatter.format(date));
    }

    /**
     * Set a URL feature in the license.
     *
     * @param name
     *            the name of the feature
     * @param url
     *            the url to store in the license
     */
    public void setFeature(final String name, final URL url) {
        setFeature(name, url.toString());
    }

    /**
     * Set an UUID feature in the license.
     *
     * @param name
     *            the name of the feature
     * @param uuid
     *            the uuid to store in the license
     */
    public void setFeature(final String name, final UUID uuid) {
        setFeature(name, uuid.toString());
    }

    @SuppressWarnings("unchecked")
    public <T> T getFeature(final String name, final Class<? extends T> klass)
            throws
            ParseException, MalformedURLException {
        T result = null;
        if (Integer.class == klass) {
            result = (T) (Integer) Integer.parseInt(getFeature(name));
        } else if (Date.class == klass) {
            result = (T) new SimpleDateFormat(DATE_FORMAT, Locale.KOREA)
                    .parse(getFeature(name));
        } else if (UUID.class == klass) {
            result = (T) UUID.fromString(getFeature(name));
        } else if (URL.class == klass) {
            result = (T) new URL(getFeature(name));
        } else if (String.class == klass) {
            result = (T) getFeature(name);
        } else {
            throw new IllegalArgumentException("'" + klass.toString()
                    + "' is not handled");
        }
        return result;
    }



    @Override
    public String toString() {
        return super.licenseProperties.toString();
    }
}
