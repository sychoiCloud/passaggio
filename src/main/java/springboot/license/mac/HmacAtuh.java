package springboot.license.mac;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class HmacAtuh {
    // hash algoism
    final private static String ALGOLISM = "HmacSHA256";
//    final private static String ALGOLISM = "HmacSHA512";

    public static String getSignature(byte[] key, String payload) {
        try {
            // Get instance by algoism
            Mac hasher = Mac.getInstance(ALGOLISM);

            // Initialize instance with secret key
            hasher.init(new SecretKeySpec(key, ALGOLISM));

            // doFinal
            byte[] hash = hasher.doFinal(payload.getBytes());

            return DatatypeConverter.printHexBinary(hash).toLowerCase();
        }
        catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        catch (InvalidKeyException e){
            e.printStackTrace();
        }
        return "";
    }

    public static boolean verifySignature(byte[] key, String payload, String signature) throws RuntimeException{
        boolean verified = true;
        if(signature == null || "".equals(signature)){
            throw new RuntimeException("Signature is Empty.");
        }

        final String resultSignature = getSignature(key, payload);
        final String resultSignature2 = getSignature(key, payload);
        if("".equals(resultSignature)){
            throw new RuntimeException("The signature to compare is empty.");
        }

        if (resultSignature.equals(signature)){
            return true;
        } else {
            return false;
        }
    }
}
