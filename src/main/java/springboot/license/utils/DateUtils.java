package springboot.license.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static String now(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.now().format(formatter);
    }

    /**
     * 입력일자가 유효한지 체크한다.
     * @param dateStr : yyyy-MM-dd
     * @return
     */
    public static boolean dateValid(String dateStr){
        boolean dateValidity = true;
        try{
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            sf.setLenient ( false );
            Date dt = sf.parse ( dateStr );
        } catch ( ParseException e ) {
            dateValidity = false;
        } catch ( IllegalArgumentException e ) {
            dateValidity = false;
        }
        return dateValidity;
    }


    /**
     * dateStr으로 Date형으로 변환하여 리턴한다.
     * @param dateStr : yyyy-MM-dd
     * @return
     */
    public static Date strToDate(String dateStr){
        Date dt = null;
        try{
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            sf.setLenient ( false );
            dt = sf.parse ( dateStr );
        } catch ( ParseException e ) {
        } catch ( IllegalArgumentException e ) {
        }
        return dt;
    }

    /**
     * Date형을 String(yyyy-MM-dd)으로 변환하여 리턴한다.
     * @param date
     * @return
     */
    public static String dateToStr(Date date){
        String dateStr = null;
        try{
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            sf.setLenient ( false );
            dateStr = sf.format(date);
        } catch ( IllegalArgumentException e ) {
        }
        return dateStr;
    }
}
