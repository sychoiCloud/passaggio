package springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import springboot.model.Client;
import springboot.model.ClientCode;
import springboot.repository.ClientCodeRepository;
import springboot.repository.ClientRepository;
import springboot.response.MessageResponse;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ClientCodeRepository clientCodeRepository;

    @Override
    public List<Client> findAll() {
        List<Client> clients = new ArrayList<>();
        clientRepository.findAll().forEach(e -> clients.add(e));
        return clients;
    }

    @Transactional
    @Override
    public ResponseEntity<MessageResponse> deleteById(Long id) {
        clientRepository.deleteById(id);
        return ResponseEntity.ok()
                .body(new MessageResponse("삭제 완료"));
    }

    @Override
    public ResponseEntity<MessageResponse> updateById(Long id, Client client) {

        String todayStr = getDateTime();
        Optional<Client> e = clientRepository.findById(id);
        if (e.get() != null) {
            e.get().setCode(client.getCode());
            e.get().setName(client.getName());
            e.get().setEname(client.getEname());
            e.get().setPhone(client.getPhone());
            e.get().setEmail(client.getEmail());
            e.get().setCategory(client.getCategory());
            e.get().setRegDate(client.getRegDate());
            e.get().setModDate(todayStr);
            e.get().setFlag(client.getFlag());
        }
        clientRepository.save(e.get());

        return ResponseEntity.ok()
                .body(new MessageResponse("수정 완료"));
    }

    @Override
    public Client save(Client client) {
        // 고객코드 생성
        String firstCode = "";
        String secondCode = "";

        String category = client.getCategory();
        switch (category) {
            case "공공":
                firstCode = "G";
                break;
            case "민간":
                firstCode = "P";
                break;
            case "금융":
                firstCode = "F";
                break;
            case "해외":
                firstCode = "O";
                break;
        }
        secondCode = client.getEname().substring(0, 1);
        secondCode = secondCode.toUpperCase();
        String code = firstCode + secondCode;
        ClientCode clientCode = clientCodeRepository.findByCategory(code);
        int no = 0;
        if(clientCode != null) {
            no = clientCode.getCodeNo() + 1;
            clientCode.setCodeNo(no);
        }
        else {
            clientCode = new ClientCode();
            no = (int)(Math.random() * 20 + 120);
            clientCode.setCategory(code);
            clientCode.setCodeNo(no);
        }
        code += Integer.toString(no);

        clientCodeRepository.save(clientCode);
        String nospaceName = client.getName().replaceAll(" ", "");
        client.setNameNospace(nospaceName);
        client.setCode(code);
        String todayStr = getDateTime();
        client.setRegDate(todayStr);

        clientRepository.save(client);

        return client;
    }

    @Override
    public Client getByName(String name) {
        Client client = clientRepository.findByName(name);

        return client;
    }

    @Override
    public List<String> getNameList() {
        List<Client> products = findAll();
        List<String> nameList = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            nameList.add(products.get(i).getName());
        }
        return nameList;
    }

    @Override
    public String getDateTime() {
        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date = new Date();
        transFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        String todayStr = transFormat.format(date);
        return todayStr;
    }

    @Override
    public ResponseEntity<MessageResponse> dupCheck(String name) {
        String nospaceName = name.replaceAll(" ", "");
        nospaceName = nospaceName.replaceAll("\\p{Z}", "");
        Client client = clientRepository.findByNameNospace(nospaceName);

        if (client == null) {
           return ResponseEntity
                   .ok()
                   .body(new MessageResponse("Success : 중복이 아닙니다"));
        }

        return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error : 이름이 중복입니다."));
    }
}
