package springboot.service;

import org.springframework.http.ResponseEntity;
import springboot.model.User;
import springboot.response.MessageResponse;

public interface EmailService {

    ResponseEntity<MessageResponse> sendSignUpAuthMessage(String to, String id, String subject);
    ResponseEntity<MessageResponse> sendPasswordResetMessage(String to, String id, String subject);
}
