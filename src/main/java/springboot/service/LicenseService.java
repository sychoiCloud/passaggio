package springboot.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;
import springboot.model.License;
import springboot.model.LicenseHistory;

public interface LicenseService {

    List<License> getAll();
    List<License> getById(String id);

    ResponseEntity<String> updateById(Long id, License license);
    ResponseEntity<String> updateRequestById(License license);
    ResponseEntity<String> deleteById(Long id);

    License saveIssue(License licenses);
    License saveAdminIssue(License licenses) throws ParseException, IOException;
    void saveHistory(License license, String todayStr);

    License accept(License license) throws IOException, ParseException;
    License reject(License license);
    License cancel(License license);
    void checkLicenseStartDate(String today) throws ParseException, IOException;
    void checkLicenseExpDate(String today) throws ParseException, IOException;
    String readFile(String path, Charset encoding) throws IOException;

    License getByNames(Long id, String userId, String productName, String clientName);
    List<License> getByProcessStatus(String processStatus);
    List<LicenseHistory> getHistoryByProcessStatus(String processStatus);

    String generateLicense(License license) throws IOException;

    String getDateTime();
}
