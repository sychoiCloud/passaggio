package springboot.service;

import org.springframework.http.ResponseEntity;
import springboot.model.Client;
import springboot.response.MessageResponse;

import java.util.List;

public interface ClientService {
    List<Client> findAll();

    ResponseEntity<MessageResponse> deleteById(Long id);

    ResponseEntity<MessageResponse> updateById(Long id, Client client);

    Client save(Client client);

    Client getByName(String name);

    List<String> getNameList();

    String getDateTime();

    ResponseEntity<MessageResponse> dupCheck(String name);
}
