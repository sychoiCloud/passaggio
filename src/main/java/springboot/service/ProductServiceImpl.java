package springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import springboot.model.Product;
import springboot.repository.ProductRepository;
import springboot.response.MessageResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> findAll() {
        List<Product> Products = new ArrayList<>();
        productRepository.findAll().forEach(e -> Products.add(e));
        return Products;
    }

    @Override
    public Product getByName(String name) {
        Product product = productRepository.findByName(name);

        return product;
    }

    @Override
    public List<String> getNameList() {
        List<Product> products = findAll();
        List<String> nameList = new ArrayList<>();
        for (int i=0;i < products.size(); i++) {
            nameList.add(products.get(i).getName());
        }
        return nameList;
    }

    @Override
    public ResponseEntity<MessageResponse> updateById(Long id, Product product) {
        Optional<Product> e = productRepository.findById(id);
        e.get().setName(product.getName());
        e.get().setLatestVersion(product.getLatestVersion());

        productRepository.save(e.get());

        return ResponseEntity.ok()
                .body(new MessageResponse("삭제 완료"));
    }

    @Override
    public ResponseEntity<MessageResponse> deleteById(Long id) {
        productRepository.deleteById(id);

        return ResponseEntity.ok()
                .body(new MessageResponse("삭제 완료"));
    }
}

