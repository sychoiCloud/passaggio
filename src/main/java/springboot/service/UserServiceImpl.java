package springboot.service;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import springboot.dto.UserDto;
import springboot.model.*;
import springboot.repository.RoleRepository;
import springboot.repository.UserHistoryRepository;
import springboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.response.MessageResponse;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserHistoryRepository userHistoryRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public List<User> getAll() {
        List<User> users = userRepository.findAllByOrderByRegDateDesc();
        List<User> users2 = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getStatus().equals("가입 요청")) continue;
            if (users.get(i).getId().equals("maestro")) continue;

            users2.add(users.get(i));
        }

        return users2;
    }

    @Override
    public List<User> findAllOrderByRegDate() {
        List<User> userList = userRepository.findAllByOrderByRegDateDesc();
        for (int i = 0;i < userList.size(); i++){
            if (userList.get(i).getId().equals("maestro")) {
                userList.remove(i);
                return userList;
            }
        }

        return userList;
    }

    @Override
    public ResponseEntity<MessageResponse> deleteById(String id) {
        userRepository.deleteById(id);

        return ResponseEntity.ok()
                .body(new MessageResponse("삭제 완료"));
    }

    @Override
    public ResponseEntity<MessageResponse> updateById(String id, User user) {

        String todayStr = getDateTime();
        Optional<User> e = userRepository.findById(id);

        e.get().setPassword(passwordEncoder.encode(user.getPassword()));
        e.get().setUsername(user.getUsername());
        e.get().setPhone(user.getPhone());
        e.get().setEmail(user.getEmail());
        e.get().setRegDate(user.getRegDate());
        e.get().setModDate(todayStr);
        e.get().setFlag(user.getFlag());

        userRepository.save(e.get());

        return ResponseEntity.ok()
                .body(new MessageResponse("Success : 정보 변경 완료"));
    }

    @Override
    public ResponseEntity<MessageResponse> checkPasswd(String id, User user) {
        Optional<User> originUser = userRepository.findById(id);

        if (passwordEncoder.matches(user.getPassword(), originUser.get().getPassword())) {
            return ResponseEntity
                    .ok()
                    .body(new MessageResponse("Success : 비밀번호 확인 완료"));
        }

        return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error : 비밀번호가 틀립니다"));
    }

    @Override
    public List<String> getUserIdList() {
        List<User> users = userRepository.findAll();
        List<String> idList = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            idList.add(users.get(i).getId());
        }
        return idList;
    }

    @Override
    public String login(String id, String password) {
        Optional<User> user = userRepository.findById(id);

        if (passwordEncoder.matches(password, user.get().getPassword())) {
            if (user.isPresent()) {
                if (user.get().getStatus() != "가입 요청" && user.get().getFlag() != "N") {
                    String token = UUID.randomUUID().toString();
                    User custom = user.get();
                    custom.setToken(token);
                    userRepository.save(custom);
                    return token;
                }
            }
        }

//        초기 관리자 세팅용
//        ------------------------------------------------------------
//        User user = userRepository.findByIdAndPassword(id, password);
//
//        if (user != null) {
//            if (user.getStatus() != "가입 요청") {
//                String token = UUID.randomUUID().toString();
//                User custom = user;
//                custom.setToken(token);
//                custom.setPassword(passwordEncoder.encode(user.getPassword()));
//                userRepository.save(custom);
//                return token;
//            }
//        }
//        ------------------------------------------------------------


        return StringUtils.EMPTY;
    }

    @Override
    public Optional<org.springframework.security.core.userdetails.User> findByToken(String token) {
        User user = userRepository.findByToken(token);
        if (user != null) {
            List<GrantedAuthority> authorities = new ArrayList<>();

            authorities.add(new SimpleGrantedAuthority(user.getAuthority()));

            org.springframework.security.core.userdetails.User user2 = new org.springframework.security.core.userdetails.User(user.getId(), user.getPassword(), authorities);

            return Optional.of(user2);
        }
        return Optional.empty();
    }

    @Override
    public List<User> getByProcessStatus(String processStatus) {

        List<User> users = userRepository.findAllByProcessStatusOrderByRegDateDesc(processStatus);

        return users;
    }

    @Override
    public List<UserHistory> getHistoryByProcessStatus(String processStatus) {

        List<UserHistory> userHistory = userHistoryRepository.findAllByProcessStatusOrderByRegDateDesc(processStatus);

        return userHistory;
    }

    // ---------------------- Register -----------------------

    @Override
    public ResponseEntity<MessageResponse> signup(User user) {

        String todayStr = getDateTime();

        if (userRepository.existsById(user.getId())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error : ID가 중복입니다. 다른 ID를 입력해주세요"));
        }
        if (userRepository.existsByEmail(user.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error : 이메일이 중복입니다. 다른 이메일을 입력해주세요"));
        }
        if (userRepository.existsByPhone(user.getPhone())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error : 전화번호가 중복입니다. 다른 전화번호를 입력해주세요"));
        }

        user.setRegDate(todayStr);
        userRepository.save(user);

        return ResponseEntity
                .ok()
                .body(new MessageResponse("Success : 회원가입 신청되었습니다"));
    }

    @Override
    public ResponseEntity<MessageResponse> accept(User user) {
        User tempUser = new User(user.getId(),
                passwordEncoder.encode(user.getPassword()),
                user.getUsername(),
                user.getPhone(),
                user.getEmail(),
                user.getRegDate(),
                user.getModDate(),
                user.getStatus(),
                user.getProcessStatus(),
                user.getFlag(),
                user.getRequest(),
                user.getAuthority(),
                user.getToken(),
                user.getRoles()
        );

        String todayStr = getDateTime();

        String strRoles = user.getRequest();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: 권한을 찾지 못했습니다"));
            roles.add(userRole);
            user.setAuthority(userRole.getName().name());
        } else {
            switch (strRoles) {
                case "admin":
                    Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: 권한을 찾지 못했습니다"));
                    roles.add(adminRole);
                    user.setAuthority(adminRole.getName().name());

                    break;
                default:
                    Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                            .orElseThrow(() -> new RuntimeException("Error: 권한을 찾지 못했습니다"));
                    roles.add(userRole);
                    user.setAuthority(userRole.getName().name());
            }
        }

        user.setModDate(todayStr);
        user.setProcessStatus("승인");

        // 히스토리 저장
        saveHistory(user);

        tempUser.setRoles(roles);
        tempUser.setRegDate(user.getRegDate());
        tempUser.setModDate(todayStr);
        tempUser.setFlag("Y");

        tempUser.setStatus("정상");
        tempUser.setProcessStatus("없음");
        tempUser.setRequest("");
        tempUser.setAuthority(user.getAuthority());
        userRepository.save(tempUser);

        return ResponseEntity.ok(new MessageResponse("처리 완료"));
    }

    @Override
    public ResponseEntity<MessageResponse> reject(User user) {

        String todayStr = getDateTime();

        user.setProcessStatus("반려");
        user.setModDate(todayStr);
        // 히스토리 저장
        saveHistory(user);

        userRepository.deleteById(user.getId());
        return ResponseEntity.ok(new MessageResponse("처리 완료"));
    }

    @Override
    public ResponseEntity<MessageResponse> cancel(User user) {

        return ResponseEntity.ok(new MessageResponse("취소 완료"));
    }

    @Override
    public ResponseEntity<MessageResponse> save(User user) {

        String todayStr = getDateTime();

        user.setRegDate(todayStr);

        userRepository.save(user);

        return ResponseEntity.ok()
                .body(new MessageResponse("생성 완료"));
    }

    @Override
    public void saveHistory(User user) {
        UserHistory userHistory = new UserHistory();
        userHistory.setId(user.getId());
        userHistory.setUsername(user.getUsername());
        userHistory.setReqDate(user.getRegDate());      // 유저 등록일시
        userHistory.setRegDate(user.getModDate());      // 유저 요청 처리일시
        userHistory.setPhone(user.getPhone());
        userHistory.setEmail(user.getEmail());
        userHistory.setStatus(user.getStatus());
        userHistory.setProcessStatus(user.getProcessStatus());
        userHistory.setRequest(user.getRequest());
        userHistoryRepository.save(userHistory);
    }

    @Override
    public String getDateTime() {
        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date = new Date();
        transFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        String todayStr = transFormat.format(date);
        return todayStr;
    }

    @Override
    public void setTempPassword(String id, String passwd) {
        Optional<User> user = userRepository.findById(id);

        if(user.isPresent()) {
            user.get().setPassword(passwordEncoder.encode(passwd));
        }

        userRepository.save(user.get());
    }

    @Override
    public ResponseEntity<MessageResponse> dupCheck(String id) {
        Optional<User> user = userRepository.findById(id);

        if (!user.isPresent()) {
            return ResponseEntity
                    .ok()
                    .body(new MessageResponse("Success : 중복이 아닙니다"));
        }

        return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error : ID가 중복입니다."));
    }

    @Override
    public String logout(String id) {
        Optional<User> user = userRepository.findById(id);

        if(user.isPresent()) {
            user.get().setToken(null);
            userRepository.save(user.get());

            return "로그아웃 완료";
        }

        return "로그아웃 실패";
    }
}

