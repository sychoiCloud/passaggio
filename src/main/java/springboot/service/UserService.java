package springboot.service;

import org.springframework.http.ResponseEntity;
import springboot.model.UserHistory;
import springboot.model.User;
import springboot.response.MessageResponse;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll();
    List<User> findAllOrderByRegDate();
    ResponseEntity<MessageResponse> deleteById(String id);
    ResponseEntity<MessageResponse> updateById(String id, User user);
    ResponseEntity<MessageResponse> checkPasswd(String id, User user);

    List<String> getUserIdList();

    String login(String id, String password);
    Optional<org.springframework.security.core.userdetails.User> findByToken(String token);

    // User Request Service
    ResponseEntity<MessageResponse> signup(User user);
    ResponseEntity<MessageResponse> accept(User user);
    ResponseEntity<MessageResponse> reject(User user);
    ResponseEntity<MessageResponse> cancel(User user);

    ResponseEntity<MessageResponse> save(User user);
    void saveHistory(User user);

    String getDateTime();

    List<User> getByProcessStatus(String processStatus);
    List<UserHistory> getHistoryByProcessStatus(String processStatus);

    void setTempPassword(String id, String passwd);

    ResponseEntity<MessageResponse> dupCheck(String id);

    String logout(String id);
}
