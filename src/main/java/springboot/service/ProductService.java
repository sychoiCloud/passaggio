package springboot.service;

import org.springframework.http.ResponseEntity;
import springboot.model.Product;
import springboot.response.MessageResponse;

import java.util.List;

public interface ProductService {
    List<Product> findAll();

    Product getByName(String name);

    List<String> getNameList();

    ResponseEntity<MessageResponse> updateById(Long id, Product product);
    ResponseEntity<MessageResponse> deleteById(Long id);

}
