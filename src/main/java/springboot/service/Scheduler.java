package springboot.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {

    @Autowired
    LicenseService licenseService;

    @Scheduled(cron = "59 * * * * *")
    public void cronJobSch() throws IOException, ParseException {
        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date = new Date();
        transFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        String todayStr = transFormat.format(date);

        licenseService.checkLicenseStartDate(todayStr);
        licenseService.checkLicenseExpDate(todayStr);
        System.out.println("스케줄링 실행완료");
    }
}