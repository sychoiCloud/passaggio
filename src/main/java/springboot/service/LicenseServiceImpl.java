package springboot.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import springboot.license.LicenseGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springboot.model.*;
import springboot.repository.LicenseRepository;
import springboot.repository.LicenseHistoryRepository;
import springboot.repository.UserRepository;

import javax.transaction.Transactional;
import javax.xml.ws.Response;

@Slf4j
@Service
@AllArgsConstructor
public class LicenseServiceImpl implements LicenseService {

    private LicenseRepository licensesRepository;
    private LicenseHistoryRepository licensesHistoryRepository;
    private UserRepository userRepository;
    private ClientService clientService;
    private ProductService productService;

    @Override
    public List<License> getAll() {
        List<License> licenses = licensesRepository.findAllByOrderByRegDateDesc();
        List<License> licenses2 = new ArrayList<>();
        for (int i=0; i < licenses.size(); i++) {
            if (licenses.get(i).getStatus().equals("발급 요청")) continue;
            licenses2.add(licenses.get(i));
        }

        return licenses2;
    }

    @Override
    public List<License> getById(String id) {
        Optional<User> user =  userRepository.findById(id);
        List<License> license = licensesRepository.findAllByUserOrderByRegDateDesc(user.get());
        return license;
    }

    @Transactional
    @Override
    public ResponseEntity<String> deleteById(Long id) {
        licensesRepository.deleteById(id);

        return ResponseEntity.ok()
                .body("삭제 완료");
    }

    @Override
    public License saveIssue(License license) {

        String todayStr = getDateTime();

        Product product = productService.getByName(license.getProduct().getName());
        license.setProduct(product);
        license.setRegDate(todayStr);
        license.setModDate(todayStr);

        licensesRepository.save(license);

        log.info("라이센스 ID(" + license.getId() + ") : " + license.getStatus() + "이 등록 되었습니다.");

        return license;
    }

    @Override
    public License saveAdminIssue(License license) throws ParseException, IOException {

        String todayStr = getDateTime();

        Product product = productService.getByName(license.getProduct().getName());
        license.setProduct(product);
        license.setRegDate(todayStr);
        license.setModDate(todayStr);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date today = sdf.parse(todayStr);
        Date stDay = sdf.parse(license.getStDate());

        if (today.compareTo(stDay) >= 0) {
            String licenseStr = generateLicense(license);
            license.setContentRead(licenseStr);

            byte[] licenseContent = licenseStr.getBytes("UTF-8");
            license.setContent(licenseContent);

            license.setStatus("정상");
            license.setFlag("Y");
        }
        else {
            license.setStatus("예약");
            license.setFlag("N");
        }

        licensesRepository.save(license);

        log.info("라이센스 ID(" + license.getId() + ") : " + license.getStatus() + "이 등록 되었습니다.");

        return license;
    }

    @Override
    public License accept(License license) throws IOException, ParseException {

        String todayStr = getDateTime();

        // 라이센스 히스토리 저장
        license.setProcessStatus("승인");
        saveHistory(license, todayStr);
        license.setModDate(todayStr);

        log.info("라이센스 ID(" + license.getId() + ") : " + license.getStatus() + "이 " + license.getProcessStatus() + " 되었습니다.");

        // 라이센스 케이스별 상태 변경 후 저장
        if (license.getStatus().equals("발급 요청")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date today = sdf.parse(todayStr);
            Date stDay = sdf.parse(license.getStDate());

            if (today.compareTo(stDay) >= 0) {
                String licenseStr = generateLicense(license);
                license.setContentRead(licenseStr);

                byte[] licenseContent = licenseStr.getBytes("UTF-8");
                license.setContent(licenseContent);

                license.setStatus("정상");
                license.setFlag("Y");
            }
            else {
                license.setStatus("예약");
                license.setFlag("N");
            }
        }
        else if(license.getStatus().equals("폐기 요청")) {
            license.setStatus("폐기");
            license.setFlag("N");
        }
        else if(license.getStatus().equals("갱신 요청")) {
            Optional<License> li = licensesRepository.findById(license.getId());

            li.get().setStatus("정상");
            li.get().setFlag("Y");
            li.get().setExpDate(license.getRequest());
            li.get().setRequest("");

            license = li.get();
        }

        // 라이센스 상태 저장
        license.setProcessStatus("없음");

        licensesRepository.save(license);

        return license;
    }

    @Override
    public License reject(License license) {

        String todayStr = getDateTime();

        // 라이센스 히스토리 저장
        license.setProcessStatus("반려");
        saveHistory(license, todayStr);
        license.setModDate(todayStr);

        switch(license.getStatus()) {
            case "발급 요청":
                licensesRepository.deleteById(license.getId());
                // TODO : 메시징 작업
                return license;
            case "갱신 요청":
                license.setRequest("");
                // TODO : 메시징 작업
                break;
            case "폐기 요청":
                // TODO : 메시징 작업
                break;
        }

        // 라이센스 상태 저장
        license.setStatus("정상");
        license.setProcessStatus("없음");

        licensesRepository.save(license);


        return license;
    }

    @Override
    public License cancel(License license) {

        String todayStr = getDateTime();

        // 라이센스 히스토리 저장
        license.setProcessStatus("취소");
        saveHistory(license, todayStr);
        license.setModDate(todayStr);

        switch(license.getStatus()) {
            case "발급 요청":
                licensesRepository.deleteById(license.getId());
                // TODO : 메시징 작업
                return license;
            case "갱신 요청":
                license.setRequest("");
                // TODO : 메시징 작업
                break;
            case "폐기 요청":
                // TODO : 메시징 작업
                break;
        }
        license.setStatus("정상");
        license.setProcessStatus("없음");

        licensesRepository.save(license);

        return license;
    }

    @Override
    public void checkLicenseStartDate(String today) throws ParseException, IOException {
        List<License> licenseList = licensesRepository.findAll();

        for (int i = 0; i < licenseList.size(); i++) {
            if (!licenseList.get(i).getStatus().equals("예약")) {
                continue;
            }

            String stDateStr = licenseList.get(i).getStDate();
            SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            transFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

            Date stDate = transFormat.parse(stDateStr);
            Date todayDate = transFormat.parse(today);
            String todayDateTimeStr = getDateTime();

            if (todayDate.compareTo(stDate) < 0) {
                continue;
            }

            String licenseStr = generateLicense(licenseList.get(i));
            licenseList.get(i).setContentRead(licenseStr);

            byte[] licenseContent = licenseStr.getBytes("UTF-8");
            licenseList.get(i).setContent(licenseContent);

            licenseList.get(i).setStatus("정상");
            licenseList.get(i).setFlag("Y");
            licenseList.get(i).setModDate(todayDateTimeStr);

            licensesRepository.save(licenseList.get(i));
        }
    }

    @Override
    public void checkLicenseExpDate(String today) throws ParseException, IOException {
        List<License> licenseList = licensesRepository.findAll();

        for (int i = 0; i < licenseList.size(); i++) {
            if (licenseList.get(i).getStatus() == "폐기" || licenseList.get(i).getStatus() == "만료") {
                continue;
            }

            String expDateStr = licenseList.get(i).getExpDate();

            SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");

            transFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

            Date expDate = transFormat.parse(expDateStr);
            Date todayDate = transFormat.parse(today);
            String todayDateTimeStr = getDateTime();

            if (todayDate.compareTo(expDate) < 0) {
                continue;
            }

            // 오늘 날짜 > 만료일자
            licenseList.get(i).setStatus("만료");
            licenseList.get(i).setFlag("N");
            licenseList.get(i).setModDate(todayDateTimeStr);

            licensesRepository.save(licenseList.get(i));
        }
    }

    @Override
    public void saveHistory(License license, String todayStr) {

        // 라이센스 히스토리 저장
        LicenseHistory licenseHistory = new LicenseHistory(license, todayStr);
        licensesHistoryRepository.save(licenseHistory);
    }

    @Override
    public ResponseEntity<String> updateById(Long id, License license) {

        String todayStr = getDateTime();
        Optional<License> e = licensesRepository.findById(id);
//        Optional <User> user = userRepository.findById(e.get().getUser().getId());
        e.get().setRegDate(license.getRegDate());
        e.get().setModDate(todayStr);
        e.get().setExpDate(license.getExpDate());
        e.get().setStatus(license.getStatus());
        e.get().setProcessStatus(license.getProcessStatus());
        e.get().setLicenseKey(license.getLicenseKey());
        e.get().setFlag(license.getFlag());
        e.get().setType(license.getType());
        e.get().setProjectAdminEmail(license.getProjectAdminEmail());
        e.get().setProjectAdminPhone(license.getProjectAdminPhone());
        e.get().setProjectAdminName(license.getProjectAdminName());
        e.get().setProjectName(license.getProjectName());
        e.get().setContent(license.getContent());
        e.get().setHostCount(license.getHostCount());
        e.get().setModel(license.getModel());
        e.get().setPartNo(license.getPartNo());

//        e.get().setProductVersion(license.getProductVersion());

//        e.get().setUser(user.get());
//        e.get().setProduct(license.getProduct());
        e.get().setClient(license.getClient());

        licensesRepository.save(e.get());

        return ResponseEntity.ok()
                .body("수정 완료");
    }

    @Override
    public ResponseEntity<String> updateRequestById(License license) {

        String todayStr = getDateTime();

        Optional<License> e = licensesRepository.findById(license.getId());
        e.get().setStatus(license.getStatus());
        e.get().setProcessStatus(license.getProcessStatus());
        e.get().setRequest(license.getRequest());
        e.get().setModDate(todayStr);


        licensesRepository.save(e.get());

        return ResponseEntity.ok()
                .body("수정 완료");
    }

    @Override
    public License getByNames(Long id, String userId, String productName, String clientName) {
        Optional<User> user = userRepository.findById(userId);
        Product product = productService.getByName(productName);
        Client client = clientService.getByName(clientName);

        Optional<License> license = licensesRepository.findById(id);
        license.get().setUser(user.get());
        license.get().setProduct(product);
        license.get().setClient(client);

        return license.get();
    }

    @Override
    public List<License> getByProcessStatus(String processStatus) {

        List<License> licenses = licensesRepository.findAllByProcessStatusOrderByRegDateDesc(processStatus);
//        for (int i = 0; i < licenses.size(); i++) {
//            if (licenses.get(i).getRegDate() != null) {
//                licenses.get(i).setRegDate(licenses.get(i).getRegDate().substring(0, 10));
//            }
//        }

        return licenses;
    }

    @Override
    public List<LicenseHistory> getHistoryByProcessStatus(String processStatus) {

        List<LicenseHistory> licenseHistories = licensesHistoryRepository.findAllByProcessStatusOrderByRegDateDesc(processStatus);

        return licenseHistories;
    }

    @Override
    public String generateLicense(License license) throws IOException {
        String LICENSE_PATH = "";
        if(license.getProduct().getName().equals("Symphony")) {
            LICENSE_PATH = "symphony.lic";
        }
        else if(license.getProduct().getName().equals("Contrabass")) {
            LICENSE_PATH = "contrabass.lic";
        }

        LicenseGenerator generator = new LicenseGenerator();
        File f = new File(LICENSE_PATH);
        generator.generate(f, license.getLicenseKey(), license.getType(), license.getExpDate(), license.getClient().getCode(), license.getProduct().getName(), license.getModel(), license.getPartNo(), license.getHostCount(), license.getProductVersion());
        String licenseRead = readFile(LICENSE_PATH, StandardCharsets.UTF_8);

        return licenseRead;
    }

    @Override
    public String readFile(String path, Charset encoding)
        throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));

        return new String(encoded, encoding);
    }

    @Override
    public String getDateTime() {
        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date = new Date();
        transFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        String todayStr = transFormat.format(date);
        return todayStr;
    }
}