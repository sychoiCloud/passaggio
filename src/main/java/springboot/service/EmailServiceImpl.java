package springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import springboot.model.User;
import springboot.repository.UserRepository;
import springboot.response.MessageResponse;

import java.util.Random;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    public JavaMailSender emailSender;
    @Autowired
    public UserRepository userRepository;
    @Autowired
    public UserService userService;

    @Override
    public ResponseEntity<MessageResponse> sendSignUpAuthMessage(String to, String id, String subject) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);

        message.setText(new StringBuffer().append("회원가입 인증메일입니다.")
                .append("밑의 링크를 클릭하면 메일이 인증 됩니다.")
//                .append("&authKey=" + authKey + "' target='_blank'>메일 인증 링크</a>")
                .toString()
  );
        emailSender.send(message);

        return ResponseEntity
                .ok()
                .body(new MessageResponse("Success : 이메일 전송 완료"));
    }

    @Override
    public ResponseEntity<MessageResponse> sendPasswordResetMessage(String to, String id, String subject) {

        User user = userRepository.findByIdAndEmail(id, to);
        if(user != null) {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("sychoi@okestro.com");
            message.setTo(to);
            message.setSubject(subject);

            StringBuffer buffer = new StringBuffer();

            Random random = new Random();



            String chars[] = ("A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g" +
                    ",h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9").split(",");



            for (int i = 0; i < 8; i++) {

                buffer.append(chars[random.nextInt(chars.length)]);
            }

            String tempPassword = buffer.toString();

            message.setText(new StringBuffer().append("id : " + id + "님, " + "패스워드 초기화 안내입니다.\n")
                            .append("임시 비밀번호 : " + tempPassword + "\n")
                            .append("로그인 하신 후 비밀번호를 변경해주세요.\n")
                            .toString()
            );
            emailSender.send(message);

            userService.setTempPassword(id, tempPassword);

            return ResponseEntity
                    .ok()
                    .body(new MessageResponse("Success : 이메일 전송 완료"));
        }

        return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error : 존재하지 않는 정보입니다"));
    }
}